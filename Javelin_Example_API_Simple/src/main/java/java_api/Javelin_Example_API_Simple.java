package java_api;

import org.json.JSONObject;

import io.javalin.Javalin;
import io.javalin.http.Context;

/**
 * example https://javalin.io/documentation#getting-started
 * 
 * @author Leo Korbee <l.korbee@graafschapcollege.nl>
 */

public class Javelin_Example_API_Simple
{
	public static void main(String[] args)
	{

		Javalin app = Javalin.create().start(7070);

		// handles GET request http://kbepi.local:7070/get
		// ctx is the eventhandler (context => named ctx)
		String responseStr = "{ \"data\" : \"Hello World\" }";
		app.get("/get", ctx -> ctx.json(responseStr));

		// handles POST request http://kbepi.local:7070/post
		// (for example with Postman app)
		// prints to console the posted raw data
		app.post("/post", ctx -> eventHandler(ctx));

		// handles PUT request http://kbepi.local:7070/jsonput
		// (for example with Postman app)
		app.put("/jsonput", ctx -> eventHandlerJson(ctx));

		/*
		 * Put body
		 * 
		 * { "LED" : "on" }
		 * 
		 * or
		 * 
		 * { "LED" : "off" }
		 * 
		 * 
		 * 
		 */
	}

	private static void eventHandler(Context ctx)
	{
		System.out.println(ctx.body());
		// return 201
		ctx.status(201);
	}

	private static void eventHandlerJson(Context ctx)
	{
		JSONObject jobject = new JSONObject(ctx.body());

		String ledString = jobject.getString("LED");

		if (ledString.contentEquals("on"))
		{
			System.out.println("LED is on");
			ctx.json(jobject.toString());
			ctx.status(200);
		} else if (ledString.contentEquals("off"))
		{
			System.out.println("LED is off");
			ctx.json(jobject.toString());
			ctx.status(200);
		} else
		{
			// bad request response
			ctx.status(400);
		}

	}

}
