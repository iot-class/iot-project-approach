package javaio;

/*
Simple class example to show how to use Threads
https://levelup.gitconnected.com/lets-learn-java-threads-e156481883cb

@author Leo Korbee (l.korbee@graafschapcollege.nl)
@version 2022-09-24
*/

public class Threads_Example
{
	public static void main(String[] args) throws InterruptedException
	{

		// tight coupling in Thread class
		/*
		 * code is executing directly
		 */
		ThreadOne thread1 = new ThreadOne();
		thread1.start();

		// loose coupling in Runnable interface
		/*
		 * code is not executing directly, we have to create a new thread object to
		 * access the start() method
		 */
		ThreadTwo thread2 = new ThreadTwo();
		Thread subprocess = new Thread(thread2);
		subprocess.start();

		
		try
		{
			for (int i = 1; i <= 25; i++)
			{
				System.out.println("Main Thread: " + i);
				// sleep ThreadMain for one second
				Thread.sleep(250);
			}
		} catch (Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println("## Main Thread Ended ##");
		
	}

}
