package javaio;

public class ThreadOne extends Thread
{

	@Override
	public void run()
	{
		try
		{
			for (int i = 1; i <= 25; i++)
			{
				System.out.println("ThreadOne: " + i);
				// sleep ThreadOne for one second
				Thread.sleep(1000);
			}
		} catch (Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println("## ThreadOne Ended ##");
	}

}
