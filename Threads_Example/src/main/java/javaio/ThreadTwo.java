package javaio;

public class ThreadTwo implements Runnable
{
	@Override
	public void run()
	{
		try
		{
			for (int i = 1; i <= 25; i++)
			{
				System.out.println("ThreadTwo: " + i);
				// sleep ThreadTwo for one second
				Thread.sleep(500);
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
		}
		
		System.out.println("## ThreadTwo Ended ##");
	}

}
