package api_dotmatrix_display.controller;

import java.util.ArrayList;

import api_dotmatrix_display.driver.DotMatrix;

public class DotMatrixController extends Thread
{
	
	private DotMatrix dotmatrix;
	private ArrayList<String> stringList;
	
	
	public DotMatrixController()
	{
		stringList = new ArrayList<String>();
		// fist message:
		stringList.add("Startup DotMatrix");
		dotmatrix = new DotMatrix();
	}
	
	@Override
	public void run()
	{
		for(;;)
		{
			if (stringList.size() > 0)
			{	
				dotmatrix.showMessage(stringList.get(0));
				stringList.remove(0);
			}
			// set default message when there is no data
			if (stringList.size() == 0)
			{
				dotmatrix.showMessage("Software Development");
			}
			
			try
			{
				// sleep for one second
				Thread.sleep(1000);
				System.out.println("Loop");
				
			} catch (Exception ex)
			{
				System.out.println(ex);
			}
			
		}
	}

	public void addMessage(String message)
	{
		stringList.add(message);
		dotmatrix.clearMessage();
	}
}
