package api_dotmatrix_display;

import org.json.JSONObject;

import api_dotmatrix_display.controller.DotMatrixController;
import io.javalin.Javalin;
import io.javalin.http.Context;

/**
 * example https://javalin.io/documentation#getting-started
 * 
 * @author Leo Korbee <l.korbee@graafschapcollege.nl>
 * 
 * 
 * 
 */

public class API_DotMatrix_display
{
	
	private static DotMatrixController threadDotMatrix;
	
	public static void main(String[] args)
	{

		Javalin app = Javalin.create(config -> {
			// Cross-Origin Resource Sharing (CORS) is an HTTP-header
			// based mechanism that allows a server to indicate any 
			// origins (domain, scheme, or port) other than its own 
			// from which a browser should permit loading resources
			config.enableCorsForAllOrigins();
		}).start(7070);
		

		String responseStr = "{ \"data\" : \"Hello World\" }";
		app.get("/get", ctx -> ctx.json(responseStr));

		app.post("/post", ctx -> eventHandlerJson(ctx));

		/*
		 * Post body
		 * 
		 * { "message" : "new message" }
		 * 		  
		 */
		
		 threadDotMatrix = new DotMatrixController();
		 threadDotMatrix.start();
		
	}


	private static void eventHandlerJson(Context ctx)
	{
		JSONObject jobject = new JSONObject(ctx.body());

		String message = jobject.getString("message");

		if (!message.isEmpty())
		{
			System.out.println("Message is: " + message);
			threadDotMatrix.addMessage(message);
			ctx.json(jobject.toString());
			ctx.status(200);
		} else if (message.isEmpty())
		{
			System.out.println("Message is Empty");
			ctx.json(jobject.toString());
			ctx.status(200);
		} else
		{
			// bad request response
			ctx.status(400);
		}

	}

}
