package javaio.hardware;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class LEDdriver
{
	private int ledPinNumber;
	private Pin ledPin;
	
	// Initialize the Wiring Pi Library and Hardware controller
	final static GpioController gpio = GpioFactory.getInstance();
	GpioPinDigitalOutput led;
	
	// constructor
	public LEDdriver(int ledPinNumber)
	{
		// define the pin number that has the LED connected
		//ledPin= RaspiPin.GPIO_01;
		this.ledPinNumber = ledPinNumber;
		ledPin = RaspiPin.getPinByAddress(ledPinNumber);
		led = gpio.provisionDigitalOutputPin(ledPin, "led", PinState.LOW);	
	}
	
	public int getledPinNumber()
	{
		return ledPinNumber;
	}
	
	public void setLedOn()
	{
		led.high();
	}
	
	public void setLedOff()
	{
		led.low();
	}
	
}
