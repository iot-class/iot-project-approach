package javaio;

import javaio.hardware.LEDdriver;

/*
Simple class example to show how to program driver code

@author Leo Korbee (l.korbee@graafschapcollege.nl)
@version 2022-08-07
*/

public class BlinkLED_Driver_Example
{
	public static void main(String[] args) throws InterruptedException
	{
		int ledPinNumberRed = 1;
		int ledPinNumberGreen = 2;
		
		LEDdriver redLed = new LEDdriver(ledPinNumberRed);
		LEDdriver greenLed = new LEDdriver(ledPinNumberGreen);
		
		System.out.println("Led (Red) on pin " + ledPinNumberRed + " should blink");
		System.out.println("Led (Green) on pin " + ledPinNumberGreen + " should blink");
		
		
		System.out.println("Press Ctrl-C to stop");
		
		// Loop forever until we press Ctrl-C
		while (true)
		{
			// Set the pin high (1) the pin so that the LED turns on
			redLed.setLedOn();
			greenLed.setLedOff();
			// wait 500 ms
			Thread.sleep(500);
			// Set the pin low (0) the pin so that the LED turns off
			redLed.setLedOff();
			greenLed.setLedOn();
			// wait 500 ms
			Thread.sleep(500);
		}

	}

}
